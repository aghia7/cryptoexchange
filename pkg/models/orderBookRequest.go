package models

type OrderBookRequest struct {
	CoinPair string `json:"coin_pair"`
	Limit    uint   `json:"limit"`
}
