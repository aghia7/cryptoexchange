package services

import "gitlab.com/aghia7/cryptoexchange/pkg/models"

type Service interface {
	GetOrderBook(*models.OrderBookRequest) (*models.OrderBookResponse, error)
}
