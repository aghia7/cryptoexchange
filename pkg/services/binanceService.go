package services

import (
	"gitlab.com/aghia7/cryptoexchange/pkg/models"
	"gitlab.com/aghia7/cryptoexchange/pkg/rpc"
	"log"
)

type BinanceService struct {
	rpc    rpc.RPC
	logger *log.Logger
}

func NewBinanceService(logger *log.Logger, rpc rpc.RPC) Service {
	return &BinanceService{
		logger: logger,
		rpc:    rpc,
	}
}

func (bs *BinanceService) GetOrderBook(req *models.OrderBookRequest) (*models.OrderBookResponse, error) {
	orderBookResponse, err := bs.rpc.GetOrderBook(req)

	if err != nil {
		return nil, err
	}

	return orderBookResponse, nil
}
