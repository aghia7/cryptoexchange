package rpc

import (
	"encoding/json"
	"fmt"
	"gitlab.com/aghia7/cryptoexchange/pkg/models"
	"log"
	"net/http"
	"strconv"
)

var domain = "https://www.binance.com/api/v3/"

type BinanceRPC struct {
	logger *log.Logger
}

func NewBinanceRPC(logger *log.Logger) RPC {
	return &BinanceRPC{
		logger: logger,
	}
}

type BinanceOrderBookResponse struct {
	Bids [][]string `json:"bids"`
	Asks [][]string `json:"asks"`
}

var limits = []uint{5, 10, 20, 50, 100, 500, 1000, 5000}

func (rpc *BinanceRPC) GetOrderBook(req *models.OrderBookRequest) (*models.OrderBookResponse, error) {
	var limit uint
	for i := range limits {
		if req.Limit <= limits[i] {
			limit = limits[i]
			break
		}
	}

	if limit == 0 {
		return nil, fmt.Errorf("GetOrderBook: incorrect limit")
	}
	resp, err := http.Get(fmt.Sprintf("%sdepth?symbol=%s&limit=%d", domain, req.CoinPair, limit))
	if err != nil {
		return nil, err
	}

	defer func() {
		err = resp.Body.Close()
		if err != nil {
			rpc.logger.Println("could not close response body")
		}
	}()

	var binanceOrderBookResp BinanceOrderBookResponse

	if err = json.NewDecoder(resp.Body).Decode(&binanceOrderBookResp); err != nil {
		return nil, err
	}

	if uint(len(binanceOrderBookResp.Bids)) > req.Limit {
		binanceOrderBookResp.Bids = binanceOrderBookResp.Bids[:req.Limit]
	}

	if uint(len(binanceOrderBookResp.Asks)) > req.Limit {
		binanceOrderBookResp.Asks = binanceOrderBookResp.Asks[:req.Limit]
	}

	orderBookResp, err := binanceOrderBookResp.convertToOrderBookResponse()

	if err != nil {
		return nil, err
	}

	return orderBookResp, nil
}

func (b BinanceOrderBookResponse) convertToOrderBookResponse() (*models.OrderBookResponse, error) {
	orderBookResponse := &models.OrderBookResponse{
		Bids: make([]models.OrderPair, len(b.Bids)),
		Asks: make([]models.OrderPair, len(b.Asks)),
	}

	if err := retrieveOrderPairs(orderBookResponse.Asks, b.Asks); err != nil {
		return nil, err
	}
	if err := retrieveOrderPairs(orderBookResponse.Bids, b.Bids); err != nil {
		return nil, err
	}

	return orderBookResponse, nil
}

func retrieveOrderPairs(pairs []models.OrderPair, data [][]string) error {
	var err error
	for i := range data {
		pairs[i].Price, err = strconv.ParseFloat(data[i][0], 64)
		if err != nil {
			return err
		}
		pairs[i].Amount, err = strconv.ParseFloat(data[i][1], 64)
		if err != nil {
			return err
		}
	}

	return nil
}
