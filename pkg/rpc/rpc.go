package rpc

import "gitlab.com/aghia7/cryptoexchange/pkg/models"

type RPC interface {
	GetOrderBook(*models.OrderBookRequest) (*models.OrderBookResponse, error)
}
