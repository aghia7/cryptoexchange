package handlers

import (
	"gitlab.com/aghia7/cryptoexchange/pkg/models"
	"gitlab.com/aghia7/cryptoexchange/pkg/services"
	"golang.org/x/net/websocket"
	"log"
	"sync"
	"time"
)

type OrderBookWebSocket struct {
	mutex sync.Mutex

	logger  *log.Logger
	service services.Service
}

// WebsocketOrderBookConnection handles an OrderBook websocket
func (o *OrderBookWebSocket) handleMessage(ws *websocket.Conn) {
	log.Printf("Client connected from %s", ws.RemoteAddr())
	var request *models.OrderBookRequest
	go func() {
		for {
			var req models.OrderBookRequest
			err := websocket.JSON.Receive(ws, &req)
			if err != nil {
				o.logger.Printf("Receive failed: %s; closing connection...", err.Error())
				if err = ws.Close(); err != nil {
					log.Println("Error closing connection:", err.Error())
				}
				break
			}

			o.mutex.Lock()
			request = &req
			o.mutex.Unlock()
		}
	}()

	for range time.Tick(1 * time.Second) {
		if request != nil {
			o.mutex.Lock()
			response, err := o.service.GetOrderBook(request)
			o.mutex.Unlock()

			if err != nil {
				o.logger.Println(err.Error())
				break
			}

			err = websocket.JSON.Send(ws, response)
			if err != nil {
				o.logger.Println(err.Error())
				break
			}
		}
	}
}
