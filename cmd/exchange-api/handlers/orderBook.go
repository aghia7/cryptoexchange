package handlers

import (
	"gitlab.com/aghia7/cryptoexchange/pkg/models"
	"gitlab.com/aghia7/cryptoexchange/pkg/services"
	"gitlab.com/aghia7/cryptoexchange/pkg/web"
	"log"
	"net/http"
	"strconv"
)

type OrderBookHandler struct {
	logger  *log.Logger
	service services.Service
}

func (h *OrderBookHandler) Retrieve(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet {
		err := web.RespondError(w, web.NewRequestError(nil, http.StatusMethodNotAllowed))
		if err != nil {
			h.logger.Println("Cannot respondError: " + err.Error())
		}
		return
	}

	query := r.URL.Query()

	coinPair := query.Get("coin_pair")
	limit, err := strconv.Atoi(query.Get("limit"))
	if err != nil {
		err = web.RespondError(w, web.NewRequestError(err, http.StatusBadRequest))
		if err != nil {
			h.logger.Println("Cannot respondError: " + err.Error())
		}
		return
	}

	orderBook, err := h.service.GetOrderBook(&models.OrderBookRequest{
		CoinPair: coinPair,
		Limit:    uint(limit),
	})

	if err != nil {
		err = web.RespondError(w, web.NewRequestError(err, http.StatusInternalServerError))
		if err != nil {
			h.logger.Println("Cannot respondError: " + err.Error())
		}
		return
	}

	err = web.Respond(w, orderBook, http.StatusOK)
	if err != nil {
		h.logger.Println("Cannot respond: " + err.Error())
	}
}
