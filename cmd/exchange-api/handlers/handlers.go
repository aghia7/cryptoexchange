package handlers

import (
	"gitlab.com/aghia7/cryptoexchange/pkg/rpc"
	"gitlab.com/aghia7/cryptoexchange/pkg/services"
	"golang.org/x/net/websocket"

	"log"
	"net/http"
)

func Routes(logger *log.Logger) *http.ServeMux {
	binanceRPC := rpc.NewBinanceRPC(logger)
	binanceService := services.NewBinanceService(logger, binanceRPC)
	orderBook := OrderBookHandler{logger, binanceService}
	orderBookWebSocket := OrderBookWebSocket{logger: logger, service: binanceService}

	mux := http.NewServeMux()
	mux.Handle("/orders", websocket.Handler(orderBookWebSocket.handleMessage))
	mux.HandleFunc("/api/orders", orderBook.Retrieve)
	mux.Handle("/", http.FileServer(http.Dir("static/html")))
	return mux
}
