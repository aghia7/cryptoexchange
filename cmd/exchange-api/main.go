package main

import (
	"context"
	"expvar"
	"flag"
	"gitlab.com/aghia7/cryptoexchange/cmd/exchange-api/handlers"
	"log"
	"net/http"
	_ "net/http/pprof"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/pkg/errors"
)

var (
	build           = "develop"
	apiHost         = flag.String("apihost", ":8088", "API host address")
	debugHost       = flag.String("debughost", ":8089", "Debug host address")
	readTimeout     = flag.Duration("readtimeout", 5*time.Second, "read timeout")
	writeTimeout    = flag.Duration("writetimeout", 5*time.Second, "write timeout")
	shutdownTimeout = flag.Duration("shutdowntimeout", 5*time.Second, "shutdown timeout")
)

func main() {
	logger := log.New(os.Stdout, "OrderBook API: ", log.LstdFlags|log.Lmicroseconds|log.Lshortfile)
	if err := run(logger); err != nil {
		log.Println("main: error:", err)
		os.Exit(1)
	}
}

func run(logger *log.Logger) error {
	flag.Parse()

	// =========================================================================
	// App Starting

	expvar.NewString("build").Set(build)
	log.Printf("main: Started: Application initializing: version %q", build)
	defer log.Println("main: Completed")

	// =========================================================================
	// Start Debug Service

	log.Println("main: Initializing debugging support")

	go func() {
		log.Printf("main: Debug Listening %s", *debugHost)
		if err := http.ListenAndServe(*debugHost, http.DefaultServeMux); err != nil {
			log.Printf("main: Debug Listener closed: %v", err)
		}
	}()
	// =========================================================================
	// Start API Service

	log.Println("main: Initializing API support ")
	shutdown := make(chan os.Signal, 1)
	signal.Notify(shutdown, os.Interrupt, syscall.SIGTERM)

	api := http.Server{
		Addr:         *apiHost,
		Handler:      handlers.Routes(logger),
		ReadTimeout:  *readTimeout,
		WriteTimeout: *writeTimeout,
	}

	serverErrors := make(chan error, 1)

	go func() {
		log.Printf("main: API listening on %s", api.Addr)
		serverErrors <- api.ListenAndServe()
	}()

	// =========================================================================
	// Shutdown

	select {
	case err := <-serverErrors:
		return errors.Wrap(err, "server error")
	case sig := <-shutdown:
		log.Printf("main: %v : Start shutdown", sig)
		ctx, cancel := context.WithTimeout(context.Background(), *shutdownTimeout)
		defer cancel()
		if err := api.Shutdown(ctx); err != nil {
			closeErr := api.Close()
			if closeErr != nil {
				logger.Println("could not close http server")
			}
			return errors.Wrap(err, "could not stop server gracefully")
		}
	}

	return nil
}
